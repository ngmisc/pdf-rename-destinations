/**
filesToBeMerged * Example written by Bruno Lowagie in answer to:
 * http://stackoverflow.com/questions/20131610/renaming-named-destinations-in-pdf-files
 * 
 * Searching for all the named destinations, with the purpose to rename them.
 * Change the destination for all GoTo actions from Link annotations on the first page.
 */
package saran.newgen.cos;
 
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.AcroFields.FieldPosition;
import com.itextpdf.text.pdf.AcroFields.Item;
import com.itextpdf.text.pdf.PRAcroForm;
import com.itextpdf.text.pdf.PRAcroForm.FieldInformation;
import com.itextpdf.text.pdf.PdfArray;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfDestination;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfDocument;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfPageLabels;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfString;
import com.itextpdf.text.pdf.SimpleBookmark;

import java.util.ArrayList;
import java.util.List;
import java.awt.print.Book;
import java.io.File;
import java.util.Iterator;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
 



public class PDFRenDest{
	public int NamedDestDeep=0;
	public int BookMarkDeep=0;
	public Map<String,PdfString> renamed = new HashMap<String,PdfString>();
    public static void main(String[] args) throws IOException, DocumentException{
        PDFRenDest app=new PDFRenDest();
        
    	if(args[0].equals("merge")){
	    		if(args.length!=3){
	            	System.out.println("\n\nPlease provide both input and output PDF file full names");
	            	System.exit(0);
	            }
	        	String inFol = args[1];
	        	String destFile = args[2];
	        	app.mergeMyFiles(inFol,destFile);
    	}
    	
    	if(args[0].equals("destfix")){
    		if(args.length!=3){
            	System.out.println("\n\nPlease provide both input and output PDF file full names");
            	System.exit(0);
            }
        	String SRC = args[1];
            String DEST = args[2];
            
            File file = new File(DEST);
            File SrcFile = new File(SRC);
            if(SrcFile.exists()==false){
            	System.out.println(SRC+" does not exists.");
            	System.exit(0);
            }
            app.manipulatePdf(SRC,DEST);
    	}
    	
    	
        
        
        //Merge PDF file
        //http://developers.itextpdf.com/examples/merging-pdf-documents/merging-documents-bookmarks
        //Document document = new Document();
        //PdfCopy newPDF = new PdfCopy(new Document(),new FileOutputStream("C:/temp/a.pdf"));
        //PdfReader reader1 = new PdfReader("C:/temp/dd.pdf");
        //newPDF.addDocument(reader1);
        //document.open();
        
        
        
        System.out.println("Bookmark Deep "+app.BookMarkDeep);
        System.out.println("NamedDestination Deep "+app.NamedDestDeep);
        System.out.println("\n============================\nCompleted.\n============================");
    	System.exit(0);
    }

    
    public void changeList(List<HashMap<String,Object>>list){
    	BookMarkDeep++;
        for (HashMap<String, Object> entry : list){
            for (String key : entry.keySet()){
            	if ("Kids".equals(key)){
                    Object o=entry.get(key);
                    changeList((List<HashMap<String,Object>>)o);
                }
                else if("Named".equals(key)){
                    String dest = (String)entry.get(key);
                    String[] parts = dest.split(":");
                    if(parts.length!=3){return;}
                    String newName = parts[0]+":"+parts[1];
                    entry.put("Named", newName);
                }else{
                	
                }
            }
        }
    }

    public void changeNamedDest(PdfArray name){
        for(int i=0; i<name.size(); i+=2){
            PdfString original=name.getAsString(i);
            String namedDest=original.toString();
            String[] parts=namedDest.split(":");
            if(parts.length!=3){continue;}
            PdfString newName=new PdfString(parts[0]+":"+parts[1]);
            name.set(i,newName);
            renamed.put(original.toString(),newName);
        }
    }
    
    public void loopNamedDest(PdfArray rootkids){
    	if(rootkids==null){
    		return;
    	}
    	NamedDestDeep++;
    	System.out.println(rootkids.size());
    	for(int i=0;i<rootkids.size();i++){
            PdfDictionary destss=rootkids.getAsDict(i);
            PdfArray subkids=destss.getAsArray(PdfName.KIDS);
            loopNamedDest(subkids);
        	if(destss!=null){
	        	PdfArray namea=destss.getAsArray(PdfName.NAMES);
	        	if(namea!=null){
        			System.out.println(namea);
	        		changeNamedDest(namea);
        		}
        	}
        }
    }
    
    public void manipulatePdf(String src, String dest) throws IOException, DocumentException{
    	PdfReader reader=new PdfReader(src);
    	PdfDictionary catalog=reader.getCatalog();
        PdfDictionary names=catalog.getAsDict(PdfName.NAMES);
        PdfDictionary dests=names.getAsDict(PdfName.DESTS);
        //API to be read well
        PdfArray name=dests.getAsArray(PdfName.NAMES);
        
        if(name!=null){
        	changeNamedDest(name);
        }

      //This code collect all the PDF Named Destinations and does replacement in the names.
        //Example
        
        PdfArray rootkids=dests.getAsArray(PdfName.KIDS);
        loopNamedDest(rootkids);
        
        java.util.List bookmarks=com.itextpdf.text.pdf.SimpleBookmark.getBookmark(reader);
        changeList(bookmarks);

        String[] pagelabels=PdfPageLabels.getPageLabels(reader);
        
        //rotate and remove
        AcroFields form = reader.getAcroFields();
        HashMap formFields = (HashMap)form.getFields(); 
        Set formFieldNames = formFields.keySet(); 
        Iterator fieldNameIterator = formFieldNames.iterator(); 
        List pagestoberemoved=new ArrayList();
        List pagestoberotated=new ArrayList();
        List pagestobesaved=new ArrayList();
        String fieldName = null; 
        while(fieldNameIterator.hasNext()) 
        { 
                fieldName = (String) fieldNameIterator.next();
                List<FieldPosition> position = form.getFieldPositions(fieldName);
                for(int i=0;i<position.size();i++){
                	if(fieldName.equals("pg_delete")==true){
                		pagestoberemoved.add(position.get(i).page);
                	}
                	if(fieldName.equals("pg_rotate")==true){
                		pagestoberotated.add(position.get(i).page);
                	}
                }
        } 
        

        
        
        String missinglinks="";
        //reset links
        for(int count = 1; count<=reader.getNumberOfPages(); count++){
        	 PdfDictionary page=reader.getPageN(count);
        	 //based on pages to be rotated, rotate pages
        	 if(pagestoberotated.contains(count)==true){
        		 PdfNumber protate=page.getAsNumber(PdfName.ROTATE);
        		 if(protate==null){
        			 page.put(PdfName.ROTATE,new PdfNumber(90));
        		 }else{
        			 page.put(PdfName.ROTATE,new PdfNumber((protate.intValue() + 90) % 360));
        		 }
        	 }
        	 
        	 //if this is dummy page then remove it
        	 if(pagestoberemoved.contains(count)==false){
        		 pagestobesaved.add(count);
        	 }
        	 
        	 
             PdfArray annotations=page.getAsArray(PdfName.ANNOTS);
             if(annotations==null){
            	 continue;
             }
             PdfDictionary annotation;
             PdfDictionary action;
             PdfString n;
             PdfString URL;
             
             for(int i=0; i<annotations.size(); i++){
            	 annotation = annotations.getAsDict(i);
                 action = annotation.getAsDict(PdfName.A);
                 if(action==null){
                	 missinglinks+="<p>Page "+count+" has missing link.</p>";
                	 continue;
                	 }
                 n = action.getAsString(PdfName.D);
                 if(n==null){
                	 continue;
                	 }
                 String linkName=n.toString()+"";
                 String[] parts=linkName.split(":");
                 if(parts.length!=3){continue;}
                 action.put(PdfName.D,new PdfString(parts[0]+":"+parts[1]));                 
             }//number of links
        }//pages
        
        if(missinglinks.length()>0){
        	try{
    			File file = new File(src+".html");
    			java.io.FileWriter fw = new java.io.FileWriter(file.getAbsoluteFile());
    			java.io.BufferedWriter bw = new java.io.BufferedWriter(fw);
    			bw.write("<!DOCTYPE html><html lang='en'><head><meta charset='utf-8'><meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name='viewport' content='width=device-width, initial-scale=1'><title>Missing Links</title><link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' rel='stylesheet'></head><body style='padding:20 20 20 20;'><h1>Missing Links</h1>"+missinglinks+"<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js'></script><script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'></script></body></html>");
    			bw.close();
        	}catch(Exception fe){
        		System.out.println("Unable to write log file");
        	}
        }
        System.out.println("Pages to be removed");
        System.out.println(pagestoberemoved.toString());
        System.out.println("Pages to be rotated");
        System.out.println(pagestoberotated.toString());
        try{
        	reader.selectPages(pagestobesaved);
        	PdfStamper stamper=new PdfStamper(reader,new FileOutputStream(dest));
        	stamper.setOutlines(bookmarks);
            stamper.close();
        }catch(Exception e){
        	System.out.println("<p>Unable to save the file...</p>");
        }
        
        try{
            reader.close();
        }catch(Exception e){
            System.out.println("Unable to close the file...");
        }
    }
         
    public static void mergeMyFiles(String inDir,String destFile){
    try{
    int pageOffset = 0;
    ArrayList masterBookMarkList = new ArrayList();     
    int fileIndex = 0;
    Document document = null;
    PdfCopy writer = null;
    PdfReader reader = null;
    File sortingDetails=new File(inDir+"/sorting.txt");
    if(sortingDetails.exists()==false){
    	System.out.println("sorting.txt is not exits....");
    	System.exit(0);
    }
    String sortingText=org.apache.commons.io.FileUtils.readFileToString(sortingDetails,"UTF-8");
    String[] filesToBeMerged=sortingText.split("==");
    
    for(fileIndex=0; fileIndex<filesToBeMerged.length; fileIndex++){
    reader=new PdfReader(inDir+filesToBeMerged[fileIndex]);
    System.out.println("Reading File -"+filesToBeMerged[fileIndex]);
    
    reader.consolidateNamedDestinations();
     
    int totalPages=reader.getNumberOfPages();
    System.out.println("Checking for bookmarks...");
    List bookmarks = SimpleBookmark.getBookmark(reader);
    if(bookmarks != null){
    if(pageOffset != 0)
	    SimpleBookmark.shiftPageNumbers(bookmarks, pageOffset,null);
	    masterBookMarkList.addAll(bookmarks);
	    System.out.println("Bookmarks found and storing...");
    }else{
    	System.out.println("No bookmarks in this file...");
    }
    pageOffset += totalPages;
     

    if(fileIndex == 0){
	    document = new Document(reader.getPageSizeWithRotation(1));
	    System.out.println("Creating an empty PDF...");
	    writer = new PdfCopy(document,
	    new FileOutputStream(destFile));
	    document.open();
    }
    System.out.println("Merging File: "+filesToBeMerged[fileIndex]);
    PdfImportedPage page;
    for(int currentPage = 1; currentPage<=totalPages;currentPage++){
	    page=writer.getImportedPage(reader, currentPage);
	    writer.addPage(page);
    }
     
    System.out.println("Checking for Acroforms");
    PRAcroForm form = reader.getAcroForm();
    if(form != null){
	    writer.copyAcroForm(reader);
	    System.out.println("Acroforms found and copied");
    }else
    	System.out.println("Acroforms not found for this file");
    	System.out.println();
    }

    if(!masterBookMarkList.isEmpty()){
	    writer.setOutlines(masterBookMarkList);
	    System.out.println("All bookmarks combined and added");
    }else{
    	System.out.println("No bookmarks to add in the new file");
    }
    document.close();
    System.out.println("File has been merged and written to-");
    }catch(Exception e){
    	e.printStackTrace();
    }
    }
}